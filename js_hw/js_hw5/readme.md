Языки программирования обычно имеют дело со структурированным текстом, в котором некоторые символы (а также их комбинации)
используются в качестве управляющих, в том числе управляющих структурой текста. В ситуации, когда необходимо использовать 
такой символ в качестве «обычного символа языка», применяют экранирование.
Если нам нужно использовать специальный символ как обычный, нам необходимо добавить к нему обратную косую черту: \.